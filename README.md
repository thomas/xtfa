# Experimental Modular TFA (xTFA)

Welcome to the repository of xTFA.
xTFA is a tool for computing latency bounds in networks with cyclic dependencies, traffic regulators, packet replication and elimination functions (PREFs) and non-ideal clocks.

The main ideas as well as the underlying theoretical grounds are presented in my thesis manuscript.

## Requirements

See requirements.txt

In the current version, the implementation of the LCAN algorithm (included in the repository) requires the [gurobi software](www.gurobi.com) with a license and the [gurobipy python module](https://www.gurobi.com/documentation/9.5/quickstart_mac/cs_grbpy_the_gurobi_python.html).


## Documentation

The documentation is available at https://www.woolab.fr/xtfa

It contains a discussion on the implementation of the main ideas described in the manuscript as well at the full API reference

## Examples

chap8.py shows how to use xTFA for various configurations.
It produces the figures of Chapter 8 in my thesis
