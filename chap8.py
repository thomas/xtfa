#!/usr/bin/python3
#
# This file is part of xTFA
# Copyright (c) 2021-2022 Ludovic Thomas (ISAE-SUPAERO)
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from typing import Mapping
import matplotlib.pyplot as plt
import pandas as pd
import os

from xtfa import networks
from xtfa.clocks import Clock
from xtfa import fasUtility
from xtfa import lcan

"""
This file contains the scripts that generate the figures of chapter 8.

Note: The output of LCAN is not always deterministic because several sets of positions for the regulators could have the same costs (the ties are broken arbitrarely).
As a consequence, the results for the configurations that use regulators based on LCAN proposed might slighty differ from the results that I have included in my manuscript.
"""


def compute_file_with_cyclic_dep(in_file: str, out_file: str, out_file_node: str, **kargs):
    Clock.PERFECT = False
    Clock.SYNC = False
    if(kargs.get("ideal-clocks", False)):
        Clock.PERFECT = True
    if(kargs.get("synchronized", False)):
        Clock.SYNC = True
    #Create a network with cyclic dependencies.
    #It needs a method to identify the cuts (from fasUtility)
    #Here we select a simple method based on a topological sort
    net = networks.CyclicNetwork(fasUtility.TopologicalSort())
    #Get a reader to read the wopanet file and load the network
    reader = networks.WopanetReader()
    if("adaptation-method" in kargs.keys()):
        reader.overridingNetDict["adaptation-method"] = kargs.get("adaptation-method" )
    reader.configure_network_from_xml(net,in_file)
    #auto-installation of the pipelines
    #This auto installation is performed based on the data available inside the XML file,
    #but manual installation of the computational block in the pipelines is always possible
    
    net.auto_install_pipelines()
    #Compute
    net.compute()
    #Save results
    net.save_delay_bounds_per_destination_in_file(out_file)
    net.save_delay_bounds_per_node_in_file(out_file_node)
    print("Max usage: %.2e" % net.get_max_load())
   
def compute_then_merge_end_to_end_redundant_flows(in_file: str, out_file: str, out_file_nodes: str, **kargs):
    compute_file_with_cyclic_dep(in_file,out_file, out_file_nodes, **kargs)
    merge_end_to_end_redundant_flows(out_file)

def merge_end_to_end_redundant_flows(file: str):
    df = pd.read_csv(file, delimiter=";")  
    ndf = pd.DataFrame(columns=["Flow","Destination","Deadline","Bound"])     
    for _,row in df.iterrows():
        f = row["Flow"]
        if(not isinstance(f,str)):
            raise AssertionError()
        if(f.endswith("_R1")):
            flow = f[:-3]
            destination = row["Destination"]
            deadline = row["Deadline"]
            bound = row["Bound"] 
            for _,rrow in df.iterrows():
                if(rrow["Flow"].startswith(flow)):
                    if(rrow["Destination"] == destination):
                        bound = max(bound,rrow["Bound"])
            ndf=ndf.append(pd.DataFrame([[flow,destination,deadline,bound]],columns=["Flow","Destination","Deadline","Bound"]))
        else:
            if (not f.endswith("_R2")):
                ndf=ndf.append(row)
    ndf.to_csv(file,sep=";",index=False)
            
def break_with_lcan_then_compute(in_file: str, out_file: str, out_file_nodes: str, **kargs):
    Clock.PERFECT = False
    Clock.SYNC = False
    if(kargs.get("ideal-clocks", False)):
        Clock.PERFECT = True
    if(kargs.get("synchronized", False)):
        Clock.SYNC = True
    ir = kargs.get("ir",False)
    net = networks.CyclicNetwork(fasUtility.TopologicalSort())
    #Get a reader to read the wopanet file and load the network
    reader = networks.WopanetReader()
    reader.configure_network_from_xml(net,in_file)
    #Install model, but without regulators
    net.auto_install_pipelines()
    if(not ir):
        lcan.lcan_pfr(net,**kargs)
    else:
        lcan.lcan_ir(net,**kargs)
    net.compute()
    #Save results
    net.save_delay_bounds_per_destination_in_file(out_file)
    net.save_delay_bounds_per_node_in_file(out_file_nodes)
    print("Max usage: %.2e" % net.get_max_load())
             
def break_with_lcan_then_compute_then_merge(in_file: str, out_file: str, out_file_nodes: str, **kargs):
    break_with_lcan_then_compute(in_file, out_file, out_file_nodes, **kargs)
    merge_end_to_end_redundant_flows(out_file)
    
    
    
def compute_file_with_cyclic_dep_allow_fail(in_file: str, out_file: str, out_file_nodes: str, **kargs):
    try:
        compute_file_with_cyclic_dep(in_file, out_file, out_file_nodes, **kargs)
    except AssertionError as e:
        print("[FAIL] Could not compute latency bounds because:")
        print("[FAIL] %s" % e)
        
    


    
available_studies = {
    "RedundancyAtEdges": {
        "infile": "0-data/volvo-topologies/redundancy-at-edges/volvo-redundancy-at-edges.xml",
        "call" : compute_then_merge_end_to_end_redundant_flows,
        "kargs": {
            "ideal-clocks" : True
        },
        "outfile": "1-results/volvo/ete-bounds/redundancy-at-edges/no-regulator.csv",
        "outfile-per-node": "1-results/volvo/bounds-per-node/redundancy-at-edges/no-regulator.csv",
        "plot-label": "Redundancy only at stations",
        "plot-options": {
            "color": "C0",
            "marker": "+",
            "linestyle": "dashed"
        }
    },
    "RedundancyAtEdgesLcanPfr": {
        "infile": "0-data/volvo-topologies/redundancy-at-edges/volvo-redundancy-at-edges.xml",
        "call" : break_with_lcan_then_compute_then_merge,
        "kargs": {
            "ideal-clocks" : True
        },
        "outfile": "1-results/volvo/ete-bounds/redundancy-at-edges/lcan-pfr.csv",
        "outfile-per-node": "1-results/volvo/bounds-per-node/redundancy-at-edges/lcan-pfr.csv",
        "plot-label": "Partial-deployment of PFRs with LCAN",
        "plot-options": {
            "color": "C1",
            "marker": "o",
            "linestyle": "none",
            "fillstyle" : "none"
        }
    },
    "PrefTightModelPfrLcan": {
        "infile": "0-data/volvo-topologies/pref/no-reg-after-pef/tight-model.xml",
        "call" : break_with_lcan_then_compute_then_merge,
        "kargs": {
            "ideal-clocks" : True,
            "ir" : False
        },
        "outfile": "1-results/volvo/ete-bounds/pref/no-reg-after-pef/tight-model-lcan-pfr.csv",
        "outfile-per-node": "1-results/volvo/bounds-per-node/pref/no-reg-after-pef/tight-model-lcan-pfr.csv",
        "plot-label": "Partial-deployment of PFRs with LCAN",
        "plot-options": {
            "color": "C4",
            "marker": "o",
            "fillstyle" : "none",
            "linestyle": "none"
        }
    },
    "PrefTightModel": {
        "infile": "0-data/volvo-topologies/pref/no-reg-after-pef/tight-model.xml",
        "call" : compute_file_with_cyclic_dep,
        "kargs": {
            "ideal-clocks" : True
        },
        "outfile": "1-results/volvo/ete-bounds/pref/no-reg-after-pef/tight-model-no-regulator.csv",
        "outfile-per-node": "1-results/volvo/bounds-per-node/pref/no-reg-after-pef/tight-model-no-regulator.csv",
        "plot-label": "Without regulator",
        "plot-options": {
            "color": "C2",
            "marker": "+",
            "linestyle": "dashed"
        }
    },
    "PrefTightModelClocks": {
        "infile": "0-data/volvo-topologies/pref/no-reg-after-pef/tight-model.xml",
        "call" : compute_file_with_cyclic_dep,
        "kargs": {
            "ideal-clocks" : False,
            "synchronized" : False
        },
        "outfile": "1-results/volvo/ete-bounds/pref/no-reg-after-pef/tight-model-no-regulator-clocks.csv",
        "outfile-per-node": "1-results/volvo/bounds-per-node/pref/no-reg-after-pef/tight-model-no-regulator-clocks.csv",
        "plot-label": "Redundancy Mechanisms + Non-Ideal Clocks",
        "plot-options": {
            "color": "C3",
            "marker": "o",
            "linestyle": "none",
            "fillstyle" : "none"
        }
    },
    "PrefIntuitiveApproach": {
        "infile": "0-data/volvo-topologies/pref/no-reg-after-pef/intuitive-approach.xml",
        "call" : compute_file_with_cyclic_dep,
        "kargs": {
            "ideal-clocks" : True
        },
        "outfile": "1-results/volvo/ete-bounds/pref/no-reg-after-pef/intuitive-no-regulator.csv",
        "outfile-per-node": "1-results/volvo/bounds-per-node/pref/no-reg-after-pef/intuitive-no-regulator.csv",
        "plot-label": "Intuitive approach of PREFs",
        "plot-options": {
            "color": "C3",
            "fillstyle": "none",
            "marker": "o",
            "linestyle": "none"      
        }
    },
    "PrefIntuitiveApproachPfr": {
        "infile": "0-data/volvo-topologies/pref/no-reg-after-pef/intuitive-approach.xml",
        "call" : break_with_lcan_then_compute_then_merge,
        "kargs": {
            "ideal-clocks" : True,
            "ir" : False
        },
        "outfile": "1-results/volvo/ete-bounds/pref/no-reg-after-pef/intuitive-lcan-pfr.csv",
        "outfile-per-node": "1-results/volvo/bounds-per-node/pref/no-reg-after-pef/intuitive-lcan-pfr.csv",
        "plot-label": "Intuitive approach + LCAN PFR",
        "plot-options": {
            "color": "C9",
            "fillstyle": "none",
            "marker": "+",
            "linestyle": "none"      
        }
    },
    "PrefWithIR": {
        "infile": "0-data/volvo-topologies/pref/reg-after-pef/pef-ir.xml",
        "call" : compute_file_with_cyclic_dep_allow_fail,
        "kargs": {
            "ideal-clocks" : True
        },
        "outfile": "1-results/volvo/ete-bounds/pref/reg-after-pef/pef-ir.csv",
        "outfile-per-node": "1-results/volvo/bounds-per-node/pref/reg-after-pef/pef-ir.csv",
        "plot-label": "PEF + ATS",
        "plot-options": {
            "color": "C9",
            "marker": "o",
            "linestyle": "none",
            "fillstyle" : "none"
        }
    },
    "PrefWithPfr": {
        "infile": "0-data/volvo-topologies/pref/reg-after-pef/pef-pfr.xml",
        "call" : compute_file_with_cyclic_dep,
        "kargs": {
            "ideal-clocks" : True
        },
        "outfile": "1-results/volvo/ete-bounds/pref/reg-after-pef/pef-pfr.csv",
        "outfile-per-node": "1-results/volvo/bounds-per-node/pref/reg-after-pef/pef-pfr.csv",
        "plot-label": "PEF + PFR",
        "plot-options": {
            "color": "black",
            "marker" : "x",
            "linestyle" : "none"
            
        }
    },
    "PrefWithPofAndPfr": {
        "infile": "0-data/volvo-topologies/pref/reg-after-pef/pef-pof-pfr.xml",
        "call" : compute_file_with_cyclic_dep,
        "kargs": {
            "ideal-clocks" : True
        },
        "outfile": "1-results/volvo/ete-bounds/pref/reg-after-pef/pef-pof-pfr.csv",
        "outfile-per-node": "1-results/volvo/bounds-per-node/pref/reg-after-pef/pef-pof-pfr.csv",
        "plot-label": "PEF + POF + PFR",
        "plot-options": {
            "color": "red",
            "marker": "o",
            "fillstyle": "none",
            "linestyle" : "none"
        }
    },
    "PrefWithPofAndIrClockAdam": {
        "infile": "0-data/volvo-topologies/pref/reg-after-pef/pef-pof-ir.xml",
        "call" : compute_file_with_cyclic_dep,
        "kargs": {
            "ideal-clocks" : False,
            "synchronized" : False,
            "adaptation-method": "cascade"
        },
        "outfile": "1-results/volvo/ete-bounds/pref/reg-after-pef/pef-pof-ir-adam.csv",
        "outfile-per-node": "1-results/volvo/bounds-per-node/pref/reg-after-pef/pef-pof-ir-adam.csv",
        "plot-label": "With packet ordering and adapted TSN ATS",
        "plot-options": {
            "color": "C3",
            "marker": "o",
            "linestyle": "none",
            "fillstyle" : "none"
        }
    },
        "PrefWithPofAndIr": {
        "infile": "0-data/volvo-topologies/pref/reg-after-pef/pef-pof-ir.xml",
        "call" : compute_file_with_cyclic_dep,
        "kargs": {
            "ideal-clocks" : True
        },
        "outfile": "1-results/volvo/ete-bounds/pref/reg-after-pef/pef-pof-ir.csv",
        "outfile-per-node": "1-results/volvo/bounds-per-node/pref/reg-after-pef/pef-pof-ir.csv",
        "plot-label": "With reordering and TSN ATS",
        "plot-options": {
            "color": "blue",
            "linestyle" : "none",
            "marker": "+"
        }
    },
    "PrefTightModelPfrLcanNonSyncAdam": {
        "infile": "0-data/volvo-topologies/pref/no-reg-after-pef/tight-model.xml",
        "call" : break_with_lcan_then_compute,
        "kargs": {
            "ideal-clocks" : False,
            "synchronized" : False,
            "adaptation-method": "adam"
        },
        "outfile": "1-results/volvo/ete-bounds/non-ideal-clocks/lcan-pfr/adam.csv",
        "outfile-per-node": "1-results/volvo/bounds-per-node/non-ideal-clocks/lcan-pfr/adam.csv",
        "plot-label": "ADAM",
        "plot-options": {
            "color": "C6",
            "marker": "x",
            "linestyle": "none"
        }
    },
    "PrefTightModelPfrLcanNonSyncCascade": {
        "infile": "0-data/volvo-topologies/pref/no-reg-after-pef/tight-model.xml",
        "call" : break_with_lcan_then_compute,
        "kargs": {
            "ideal-clocks" : False,
            "synchronized" : False,
            "adaptation-method": "cascade"
        },
        "outfile": "1-results/volvo/ete-bounds/non-ideal-clocks/lcan-pfr/cascade.csv",
        "outfile-per-node": "1-results/volvo/bounds-per-node/non-ideal-clocks/lcan-pfr/cascade.csv",
        "plot-label": "Rate and Burst Cascade",
        "plot-options": {
            "color": "C5",
            "fillstyle": "none",
            "marker": "^",
            "linestyle": "none"
        }
    },
    "PrefTightModelPfrLcanSync": {
        "infile": "0-data/volvo-topologies/pref/no-reg-after-pef/tight-model.xml",
        "call" : break_with_lcan_then_compute,
        "kargs": {
            "ideal-clocks" : False,
            "synchronized" : True
        },
        "outfile": "1-results/volvo/ete-bounds/non-ideal-clocks/lcan-pfr/sync.csv",
        "outfile-per-node": "1-results/volvo/bounds-per-node/non-ideal-clocks/lcan-pfr/sync.csv",
        "plot-label": "Non-adapted, synchronized network",
        "plot-options": {
            "color": "C7",
            "marker": "o",
            "linestyle": "none",
            "fillstyle" : "none"
        }
    },
    "PrefTightModelNonSync": {
        "infile": "0-data/volvo-topologies/pref/no-reg-after-pef/tight-model.xml",
        "call" : compute_file_with_cyclic_dep,
        "kargs": {
            "ideal-clocks" : False,
            "synchronized" : False
        },
        "outfile": "1-results/volvo/ete-bounds/non-ideal-clocks/no-regulator/non-sync.csv",
        "outfile-per-node": "1-results/volvo/bounds-per-node/non-ideal-clocks/no-regulator/non-sync.csv",
        "plot-label": "Non-ideal clocks, non-synchronized",
        "plot-options": {
            "color": "C2",
            "marker": "x",
            "linestyle": "none"
        }
    },
    "PrefTightModelSync": {
        "infile": "0-data/volvo-topologies/pref/no-reg-after-pef/tight-model.xml",
        "call" : compute_file_with_cyclic_dep,
        "kargs": {
            "ideal-clocks" : False,
            "synchronized" : True
        },
        "outfile": "1-results/volvo/ete-bounds/non-ideal-clocks/no-regulator/sync.csv",
        "outfile-per-node": "1-results/volvo/bounds-per-node/non-ideal-clocks/no-regulator/non-sync.csv",
        "plot-label": "Non-ideal clocks, synchronized",
        "plot-options": {
            "color": "C3",
            "marker": "o",
            "linestyle": "none",
            "fillstyle" : "none"
        }
    }
} 

def compare_studies_relative(*listStudies,**kargs):
    dataSets: Mapping[str,pd.DataFrame]
    dataSets = dict()
    mainstudy = listStudies[0]
    for study in listStudies:
        df = pd.read_csv(available_studies[study]["outfile"], delimiter=";")
        df.rename(columns={"Deadline":("Deadline_%s" % study), "Bound": ("Bound_%s" % study)}, inplace=True)
        dataSets[study] = df
    mainDf = dataSets[listStudies[0]].copy()
    for i in range(1,len(listStudies)):
        mainDf = pd.merge(mainDf, dataSets[listStudies[i]], on=["Flow","Destination"])
    mainDf.sort_values(by=["Bound_%s" % listStudies[0]],inplace=True)
    mainDf.reset_index(inplace=True)
    for s in listStudies:
        mainDf["Bound_relative_%s" % s] = (mainDf["Bound_%s" %s ] - mainDf["Bound_%s" % mainstudy])/mainDf["Bound_%s" % mainstudy]*100
    p = mainDf.plot(x="index", y=("Bound_relative_%s" % listStudies[0]),kind="scatter",marker="",figsize=(5,5))
    for study in listStudies[1:]:
        mainDf.plot(ax=p,y=("Bound_relative_%s" % study),label=available_studies[study]["plot-label"],**available_studies[study]["plot-options"])
    if(kargs.get("do_legend", True)):
        plt.legend()
    plt.xlabel("(flow, destination) index")
    plt.ylabel("Relative increase (\%)")
    #plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    # plt.xlim([0,15])
    return p

def compare_studies(*listStudies,**kargs):
    dataSets: Mapping[str,pd.DataFrame]
    dataSets = dict()
    for study in listStudies:
        df = pd.read_csv(available_studies[study]["outfile"], delimiter=";")
        df.rename(columns={"Deadline":("Deadline_%s" % study), "Bound": ("Bound_%s" % study)}, inplace=True)
        dataSets[study] = df
    mainDf = dataSets[listStudies[0]].copy()
    for i in range(1,len(listStudies)):
        mainDf = pd.merge(mainDf, dataSets[listStudies[i]], on=["Flow","Destination"])
    mainDf.sort_values(by=["Bound_%s" % listStudies[0]],inplace=True)
    if("list_flows" in kargs.keys()):
        mainDf = mainDf[mainDf["Flow"].isin(kargs["list_flows"])]
    mainDf.reset_index(inplace=True)
    p = mainDf.plot(x="index", y=("Deadline_%s" % listStudies[0]),kind="scatter",marker="",figsize=(5,5))
    for study in listStudies[:]:
        mainDf.plot(ax=p,y=("Bound_%s" % study),label=available_studies[study]["plot-label"],**available_studies[study]["plot-options"])
    if(kargs.get("do_legend", True)):
        plt.legend()
    plt.xlabel("(flow, destination) index")
    plt.ylabel("Latency bounds (s)")
    plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    plt.ylim([0,1.25e-3])
    if("xmax" in kargs.keys()):
        plt.xlim([0,kargs.get("xmax")])
    #plt.ylim([0,3e-4])
    return p

def compareStudiesOneFlowHistogram(*listStudies, **kargs):
    flow = kargs.get("flow")
    dataSets: Mapping[str,pd.DataFrame]
    dataSets = dict()
    for study in listStudies:
        df = pd.read_csv(available_studies[study]["outfile"], delimiter=";")
        df.rename(columns={"Deadline":("Deadline_%s" % study), "Bound": ("Bound_%s" % study)}, inplace=True)
        dataSets[study] = df
    mainDf = dataSets[listStudies[0]].copy()
    for i in range(1,len(listStudies)):
        mainDf = pd.merge(mainDf, dataSets[listStudies[i]], on=["Flow","Destination"])
    mainDf.sort_values(by=["Bound_%s" % listStudies[0]],inplace=True)
    if("list_flows" in kargs.keys()):
        mainDf = mainDf[mainDf["Flow"].isin(kargs["list_flows"])]
    mainDf.reset_index(inplace=True)
    
    list_values = mainDf[mainDf["Flow"].equals(flow)]
    pass

def compute_all_available_studies():
    for study in available_studies.keys():
        os.makedirs(os.path.dirname(available_studies[study]["outfile"]), exist_ok=True)
        os.makedirs(os.path.dirname(available_studies[study]["outfile-per-node"]), exist_ok=True)
        print("Computing study: %s" % study)
        available_studies[study]["call"](
            available_studies[study]["infile"],
            available_studies[study]["outfile"],
            available_studies[study]["outfile-per-node"],
            **available_studies[study]["kargs"]
        )
        
def plot_selected_studies():
    #compare_studies("RedundancyAtEdges")
    #compare_studies("RedundancyAtEdges", "PrefTightModel")
    #compare_studies("RedundancyAtEdges", "PrefTightModel", "PrefTightModelClocks")
    compare_studies("PrefTightModel", xmax=30)
    compare_studies("PrefTightModel", "PrefWithPofAndIrClockAdam", xmax=30)
    #compare_studies("PrefTightModel", "PrefTightModelPfrLcan", "PrefTightModelPfrLcanSync")
    #compareStudies("PrefTightModel", "PrefTightModelNonSync", "PrefTightModelSync")
    #compareStudiesOneFlowHistogram("PrefTightModel", "PrefWithPfr", "PrefWithPofAndPfr", "PrefWithPofAndIr", flow="C_V1_R2_B")
    #compare_studies_relative("PrefTightModelPfrLcan", "PrefTightModelPfrLcanNonSyncCascade", "PrefTightModelPfrLcanNonSyncAdam", "PrefTightModelPfrLcanSync")
    #compare_studies("PrefTightModel", "PrefWithPfr", "PrefWithPofAndPfr", "PrefWithPofAndIr", list_flows=["C_V1_R1_S","C_V1_R1_M1","C_V1_R1_M2","C_V1_R1_B","C_V1_R2_S","C_V1_R2_M1","C_V1_R2_M2","C_V1_R2_B"], xmax=7)
    plt.show()
    
if __name__ == "__main__":
    #compute_all_available_studies()
    plot_selected_studies()